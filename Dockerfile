# syntax=docker/dockerfile:1
FROM openjdk:16-alpine3.13

#install git
RUN apt-get install -y git
RUN git clone https://bitbucket.org/samxkim/sample-rest-api

#install gradle
RUN wget https://downloads.gradle-dn.com/distributions/gradle-6.5-bin.zip
RUN unzip gradle-6.5-bin.zip
ENV GRADLE_HOME /gradle-6.5
ENV PATH $PATH:/gradle-6.5/bin
#compile and run app
WORKDIR yurl
RUN gradle clean build --rerun-tasks --no-build-cache

VOLUME /tmp
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
CMD ["bundle",  "exec", "rails", "server", "-e", "production"]
EXPOSE 18181